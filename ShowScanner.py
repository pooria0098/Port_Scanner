from Extensions import services


def showConnectScan(result):
    if len(result) == 0:
        return
    else:
        result.sort()
        print("PORT\tSTATE\tSERVICE")
        for x in result:
            if str(x) in services:
                print(str(x) + "\topen\t" + services[str(x)])
            else:
                print(str(x) + "\topen\tunknown")


def showSynScan(result):
    if len(result) == 0:
        return
    else:
        result.sort()
        print("PORT\tSTATE\tSERVICE")
        for x in result:
            if str(x) in services:
                print(str(x) + "\topen\t" + services[str(x)])
            else:
                print(str(x) + "\topen\tunknown")


def showAckScan(result, method):
    if len(result) == 0:
        return
    else:
        print("PORT\tSTATE\t\tSERVICE")
        if method == 0:
            for x in result:
                if str(x) in services:
                    print(str(x) + "\tfiltered\t" + services[str(x)])
                else:
                    print(str(x) + "\tfiltered\tunknown")
        else:
            for x in result:
                if str(x) in services:
                    print(str(x) + "\tunfiltered\t" + services[str(x)])
                else:
                    print(str(x) + "\tunfiltered\tunknown")


