import argparse
import queue
import socket
import sys
import time
from threading import Thread
from Extensions import services
from PackingTcp import Packet, ip, port
from ShowScanner import showConnectScan, showSynScan, showAckScan
from ipv4 import IPv4
from tcp import TCP


def showFinScan(result):
    if len(result) == 0:
        return
    elif len(result) == len(opened_ports):
        print("All " + str(len(result)) + " scanned ports on " + args.target + "(" + str(host) + ") are open|filtered")
    else:
        result.sort()
        print("PORT\tSTATE\tSERVICE")
        for x in result:
            if str(x) in services:
                print(str(x) + "\topen\t" + services[str(x)])
            else:
                print(str(x) + "\topen\tunknown")


def showWindowsScan(no_rst, p_rst, z_rst):
    if (len(no_rst)) == 0 & (len(p_rst) == 0) & (len(z_rst) == 0):
        return
    print("PORT\tSTATE\tSERVICE")
    if len(no_rst) > 0:
        no_rst.sort()
        for x in no_rst:
            if str(x) in services:
                print(str(x) + "\tclosed\t" + services[str(x)])
            else:
                print(str(x) + "\tclosed\tunknown")
    if len(p_rst) > 0:
        p_rst.sort()
        for x in p_rst:
            if str(x) in services:
                print(str(x) + "\topen\t" + services[str(x)])
            else:
                print(str(x) + "\topen\tunknown")
    if len(z_rst) > 0:
        if len(z_rst) > len(opened_ports) / 4:
            print("All " + str(len(z_rst)) + " other scanned ports on " + args.target + "(" + str(
                host) + ") are filtered")
        else:
            z_rst.sort()
            for x in z_rst:
                if str(x) in services:
                    print(str(x) + "\tfiltered\t" + services[str(x)])
                else:
                    print(str(x) + "\tfiltered\tunknown")


end_time = start_time = time.time()
opened_ports = []  # filtered_port
second_ports = []  # unFiltered_port
rst_p = []
rst_z = []
port_queue = queue.Queue()
start_port = int()
finish_port = int()


# TCP connect() Scan [-sT] : These scans are so called because UNIX sockets programming uses a system call named
# connect() to begin a TCP connection to a remote site. If connect() succeeds, a connection was made. If it fails,
# the connection could not be made (the remote system is offline, the port is closed, or some other error occurred
# along the way).
def connectScan(dest_port):
    try:
        # SOCK_STREAM=TCP
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)
        sock.connect((host, dest_port))
    except socket.error:
        return False
    return True


def delayConn():
    while True:
        destination_port = port_queue.get()
        if connectScan(destination_port):
            opened_ports.append(destination_port)
        port_queue.task_done()


# SYN Stealth Scan [-sS] : SYN or Stealth scanning makes use of this procedure by sending a SYN packet and looking at
# the response. If SYN/ACK is sent back, the port is open and the remote end is trying to open a TCP connection. If
# the port is closed, an RST will be sent. If it is filtered, the SYN packet will have been dropped and no response
# will be sent.
def synScan():
    socketSyn = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    # Setting this socket option avoids the error Address already in use. You’ll see this when starting the server
    # and a previously used TCP socket on the same port has connections in the TIME_WAIT state.
    socketSyn.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)  # for settings of buffer size
    for destination_port in range(start_port, finish_port):
        packet = Packet(ip(), host, port(), destination_port, 0)
        socketSyn.sendto(packet.packet, (host, 0))
        time.sleep(0.1)


def delaySyn():
    # protocol: socket.ntohs(3) : 3  GGP  Gateway-to-Gateway
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    while (time.time() - start_time) <= end_time + float(delay):
        raw_data, addr = conn.recvfrom(65535)
        IPv4_header = IPv4(raw_data[14:])
        if (IPv4_header.target == ip()) & (IPv4_header.src == host):
            if IPv4_header.proto == 6:
                if (TCP(IPv4_header.data).flag_ack == 1) & (
                        TCP(IPv4_header.data).flag_syn == 1):
                    opened_ports.append(TCP(IPv4_header.data).src_port)


# ACK Scan [-sA] : this scan type sends ACK packets to a host. If an RST comes back, the port is classified
# "unfiltered" (that is, it was allowed to send its RST through whatever firewall was in place). If nothing comes
# back, the port is said to be "filtered". That is, the firewall prevented the RST coming back from the port. This
# scan type can help determine if a firewall is stateless (just blocks incoming SYN packets) or stateful (tracks
# connections and also blocks unsolicited ACK packets).
def ackScan():
    socketAck = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    socketAck.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    for destination_port in opened_ports[:]:
        packet = Packet(ip(), host, port(), destination_port, 1)
        socketAck.sendto(packet.packet, (host, 0))
        time.sleep(0.1)


def delayAck():
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    while (time.time() - start_time) <= end_time + float(delay):
        raw_data, addr = conn.recvfrom(65535)
        IPv4_header = IPv4(raw_data[14:])
        if (IPv4_header.target == ip()) & (IPv4_header.src == host):
            if IPv4_header.proto == 6:
                if TCP(IPv4_header.data).flag_rst == 1:
                    try:
                        second_ports.append(TCP(IPv4_header.data).src_port)
                    except ValueError:
                        continue


# FIN Scans [-sF] : The idea behind these type of scans is that a closed port should respond with an RST
# upon receiving packets, whereas an open port should just drop them (it’s listening for packets with SYN set).
def finScan():
    socketFin = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    socketFin.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    for destination_port in opened_ports[:]:
        packet = Packet(ip(), host, port(), destination_port, 2)
        socketFin.sendto(packet.packet, (host, 0))
        time.sleep(0.1)


def delayFin():
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    while (time.time() - start_time) <= end_time + float(delay):
        raw_data, addr = conn.recvfrom(65535)
        IPv4_header = IPv4(raw_data[14:])
        if (IPv4_header.target == ip()) & (IPv4_header.src == host):
            if IPv4_header.proto == 6:
                if TCP(IPv4_header.data).flag_rst == 1:
                    try:
                        second_ports.append(TCP(IPv4_header.data).src_port)
                    except ValueError:
                        continue


# Window Scan[-sW] : The TCP Window scan is similar to the ACK scan but can sometimes
# detect open ports as well as filtered/unfiltered ports.
def windowsScan():
    socketWin = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    socketWin.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    for destination_port in opened_ports[:]:
        packet = Packet(ip(), host, port(), destination_port, 1)
        socketWin.sendto(packet.packet, (host, 0))
        time.sleep(0.1)


def delayWindow():
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    while (time.time() - start_time) <= end_time + float(delay):
        raw_data, addr = conn.recvfrom(65535)
        IPv4_header = IPv4(raw_data[14:])
        if (IPv4_header.target == ip()) & (IPv4_header.src == host):
            if IPv4_header.proto == 6:
                if TCP(IPv4_header.data).flag_rst == 1:
                    if TCP(IPv4_header.data).CheckSum == 0:
                        rst_z.append(TCP(IPv4_header.data).src_port)
                    else:
                        rst_p.append(TCP(IPv4_header.data).src_port)


def showMenu():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target', help='IP or Host', required=True)
    parser.add_argument('-p', '--port', help='Range of Scanning : 0-100', required=True)
    parser.add_argument('-s', '--scan', help='ScanType : C(Connect Scan) - A(Ack Scan) - S(SYN Scan) - F(FIN Scan) - '
                                             'W(Windows Scan)', required=True)
    parser.add_argument('-d', '--delay', help='Delay : 7 Second')
    return parser


def C_pressed():
    try:
        for _ in range(20):
            t = Thread(target=delayConn)
            t.daemon = True
            t.start()
        for port in range(start_port, finish_port):
            port_queue.put(port)
        port_queue.join()
    except:
        print("\nShut Down")
    showConnectScan(opened_ports)


def A_pressed():
    opened_ports = list(range(start_port, finish_port))
    try:
        z = Thread(target=delayAck)
        z.start()
        y = Thread(target=ackScan)
        y.start()
        y.join()
        global end_time
        end_time = time.time() - start_time
        z.join()
    except:
        print("\nShut Down")
        sys.exit()
    if len(second_ports) < (len(opened_ports) / 2):  # unfiltered ports are second_ports
        showAckScan(second_ports, 1)
    else:  # open_ports - second_ports = filtered ports
        showAckScan(list(set(opened_ports) - set(second_ports)), 0)
    sys.exit()


def S_pressed():
    z = Thread(target=delaySyn)
    z.start()
    y = Thread(target=synScan)
    y.start()
    y.join()
    global end_time
    end_time = time.time() - start_time
    z.join()
    showSynScan(opened_ports)


def F_pressed():
    opened_ports = list(range(start_port, finish_port))
    try:
        z = Thread(target=delayFin)
        z.start()
        y = Thread(target=finScan)
        y.start()
        y.join()
        global end_time
        end_time = time.time() - start_time
        z.join()
    except:
        print("\nShut Down")
        sys.exit()
    showFinScan(list(set(opened_ports) - set(second_ports)))


def W_pressed():
    opened_ports = list(range(start_port, finish_port))
    try:
        z = Thread(target=delayWindow)
        z.start()
        y = Thread(target=windowsScan)
        y.start()
        y.join()
        global end_time
        end_time = time.time() - start_time
        z.join()
    except:
        print("\nShut Down")
        sys.exit()
    showWindowsScan(list(set(opened_ports) - (set(rst_p).union(set(rst_z)))), rst_p, rst_z)
    sys.exit()


args = showMenu().parse_args()
host = socket.gethostbyname(args.target)

if args.delay is not None:
    delay = args.delay
else:
    delay = 7

fnd = args.port.find('-')
if fnd == -1:
    if int(args.port) > 65535 or int(args.port) < 0:
        print("Port is out of range")
        sys.exit()
else:
    start_port = min(int(args.port[0:fnd]), int(args.port[fnd + 1:]))
    finish_port = max(int(args.port[0:fnd]), int(args.port[fnd + 1:]))

# START MAIN HERE
if args.scan == 'C':
    print("Starting Connect Scan ...")
    C_pressed()

elif args.scan == 'S':
    print("Starting SYN Scan ... ")
    S_pressed()

elif args.scan == 'A':
    print("Starting ACK Scan ... ")
    A_pressed()

elif args.scan == 'F':
    print("Starting FIN Scan ... ")
    F_pressed()


elif args.scan == 'W':
    print("Starting WINDOW Scan ... ")
    W_pressed()

else:
    print("Not Recognized")
    sys.exit(0)
